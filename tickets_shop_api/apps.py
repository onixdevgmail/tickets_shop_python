from django.apps import AppConfig


class TicketsShopApiConfig(AppConfig):
    name = 'tickets_shop_api'

    def ready(self):
        from .signals import pre_save_user
