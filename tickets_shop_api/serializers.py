import stripe
from django.contrib.auth.models import User
from rest_framework import serializers

from tickets_shop_api.helpers.helpers import UserSerializerHelper
from tickets_shop_api.models import Ticket, Order, OrderItem


class UserSerializer(UserSerializerHelper, serializers.ModelSerializer):
    password_confirm = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "password",
            "password_confirm"
        )
        write_only_fields = ('password', 'password_confirm')
        extra_kwargs = {
            'password': {'write_only': True},
            'password_confirm': {'write_only': True}
        }


class TicketSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ticket
        fields = '__all__'


class OrderItemSerializer(serializers.ModelSerializer):
    ticket = TicketSerializer(many=False, read_only=True)

    class Meta:
        model = OrderItem
        fields = ('ticket', 'quantity',)
        read_only_fields = ('order', 'ticket',)


class OrderSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = '__all__'

    def to_representation(self, order):
        representation = super(OrderSerializer, self).to_representation(order)
        representation['price'] = order.get_price()
        return representation


class PaySerializer(serializers.Serializer):
    token = serializers.CharField(required=True)

    def validate(self, validate_data):
        token = validate_data.get('token')
        try:
            validate_token = stripe.Token.retrieve(token)
            if validate_token.used:
                raise serializers.ValidationError({'token': 'This token is already used.'})
        except Exception as e:
            raise serializers.ValidationError({'token': 'This token is invalid.'})
        return validate_data