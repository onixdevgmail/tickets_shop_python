import datetime
import stripe
from tickets_shop import settings

stripe.api_key = settings.STRIPE_SECRET_KEY

SUCCEEDED = "succeeded"


class StripeHelper:

    @staticmethod
    def create_charge(request, result):
        order = request.user.get_user_order()
        stripe_price = order.get_stripe_price()
        try:
            response = stripe.Charge.create(
                amount=stripe_price,
                currency="usd",
                source=request.data.get('token'),
                description=order.id
            )
            if response.paid and response.status == SUCCEEDED and response.description == str(order.id) \
                    and response.amount == stripe_price:
                order.paid_at = datetime.datetime.now()
                order.transaction_id = response.id
                result["success"] = True
        except Exception as e:
            result["errors"].append(e.user_message)
        return result


