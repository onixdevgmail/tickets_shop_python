from django.contrib.auth.models import User
from rest_framework import serializers, exceptions
import django.contrib.auth.password_validation as validators


class UserSerializerHelper:

    def validate(self, data):
        password = data.get("password", None)
        password_confirm = data.get("password_confirm", None)
        if not password or not password_confirm:
            raise serializers.ValidationError(
                {"password": "Password is required"}
            )
        if password != password_confirm:
            raise serializers.ValidationError(
                {"password_confirm": "Password confirm must be the same as password"}
            )
        user = User()
        password = data.get('password')
        errors = dict()
        try:
            validators.validate_password(password=password, user=user)
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)

        if errors:
            raise serializers.ValidationError(errors)
        return data

    def create(self, validated_data):
        password = validated_data.pop("password", None)
        validated_data.pop("password_confirm", None)
        designation = validated_data.pop("designation", None)
        validated_data['username'] = validated_data.get('email')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        if designation:
            user.profile.designation = designation
            user.profile.save()
        return user

    def update(self, instance, validated_data):
        password = validated_data.pop("password", None)
        if password:
            instance.set_password(password)
        instance.save()
        return instance


class OrderHelper:

    @staticmethod
    def get_order_price(order):
        price = 0
        for item in order.items.all():
            price += item.ticket.price * item.quantity
        return price
