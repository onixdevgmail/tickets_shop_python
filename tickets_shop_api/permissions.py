from rest_framework.permissions import BasePermission


class TicketPermissions(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if user.is_authenticated:
            return True
        else:
            if view.action in ["list", "retrieve"]:
                return True
        return False

    def has_object_permission(self, request, view, obj):
        if view.action == "retrieve":
            return True
        return False


class OrderPermissions(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if user.is_authenticated:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_authenticated:
            if view.action == "retrieve":
                return obj.user == user
        return False
