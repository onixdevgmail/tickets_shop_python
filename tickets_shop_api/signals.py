from django.contrib.auth.models import User
from django.db.models.signals import pre_save
from django.dispatch import receiver


@receiver(pre_save, sender=User, dispatch_uid="save_new_user")
def pre_save_user(sender, instance, *args, **kwargs):
    instance.username = instance.email
