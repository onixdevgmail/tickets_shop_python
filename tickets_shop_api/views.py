from django.contrib.auth.models import User
from django.db.transaction import atomic
from django.http import JsonResponse
from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from tickets_shop_api.helpers.stripe_helper import StripeHelper
from tickets_shop_api.models import Ticket, Order
from tickets_shop_api.permissions import TicketPermissions, OrderPermissions
from tickets_shop_api.serializers import UserSerializer, TicketSerializer, OrderSerializer, PaySerializer, \
    OrderItemSerializer


class UserViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @atomic
    def perform_create(self, serializer):
        super().perform_create(serializer)


class TicketViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    permission_classes = (TicketPermissions,)

    @action(methods=["post"], detail=True, name="add_to_order")
    def add_to_order(self, request, pk):
        serializer = OrderItemSerializer(data=request.data)
        result = {"success": False, "errors": []}
        if serializer.is_valid():
            ticket = get_object_or_404(Ticket, pk=int(pk))
            ticket.add_to_cart(request.data, request.user)
            result = {"success": True, "order": OrderSerializer(request.user.get_user_order(), many=False).data}
        else:
            result["errors"] = serializer.errors
        return Response(result)


class OrderViewSet(mixins.RetrieveModelMixin, GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (OrderPermissions,)

    @action(methods=["post"], detail=False, name="pay")
    def pay(self, request):
        serializer = PaySerializer(data=request.data)
        result = {"success": False, "errors": []}
        if serializer.is_valid():
            if request.user.get_user_order().get_price() >= 1:
                result = StripeHelper.create_charge(request, result)
            else:
                result["errors"] = ["price must be greater than or equal to 1"]
        else:
            result["errors"] = serializer.errors
        return Response(result)

    def list(self, request, *args, **kwargs):
        queryset = request.user.get_user_order()
        if queryset:
            serializer = self.get_serializer(queryset, many=False)
            return Response(serializer.data)
        return Response({})
