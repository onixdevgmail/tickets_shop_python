from django.contrib.auth.models import User
from django.db import models
from django.contrib import auth

from tickets_shop_api.helpers.helpers import OrderHelper

User._meta.get_field("email")._unique = True


def get_user_order(self):
    return self.orders.filter(is_paid=False, is_active=True).first()


auth.models.User.add_to_class('get_user_order', get_user_order)


class Ticket(models.Model):
    event_name = models.CharField(max_length=100, null=False, blank=False)
    event_description = models.TextField(max_length=30000, null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def add_to_cart(self, data, user):
        quantity = data.get('quantity')
        order = user.get_user_order()
        if order:
            order_item = OrderItem.objects.create(quantity=quantity, order=order, ticket=self)
        else:
            order = Order.objects.create(user=user)
            order_item = OrderItem.objects.create(quantity=quantity, order=order, ticket=self)
        return order_item

    def __str__(self):
        return self.event_name


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='orders')
    is_active = models.BooleanField(default=True)
    is_paid = models.BooleanField(default=False)
    paid_at = models.DateTimeField(blank=True, null=True)
    transaction_id = models.IntegerField(null=True)

    def get_price(self):
        return OrderHelper.get_order_price(self)

    def get_stripe_price(self):
        price = self.get_price() * 100
        return int(price)

    def __str__(self):
        return self.user.username


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='items')
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, related_name='order_items')
    quantity = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return '{} tickets for {}, buyer: {}'.format(self.quantity, self.ticket, self.order.user.username)
