from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from tickets_shop_api.models import Ticket, Order, OrderItem


class UserCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name',)


class UserAdminClass(UserAdmin):
    add_form = UserCreateForm

    add_fieldsets = (
        (None, {
            'fields': ('email', 'password1', 'password2',),
        }),
    )


admin.site.unregister(User)
admin.site.register(User, UserAdminClass)
admin.site.register(Ticket)
admin.site.register(Order)
admin.site.register(OrderItem)
