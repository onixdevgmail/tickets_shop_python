from django.conf.urls import url
from rest_framework import routers
from django.urls import path, include
from tickets_shop_api.views import UserViewSet, TicketViewSet, OrderViewSet

router = routers.DefaultRouter()
router.register(r"users", UserViewSet, basename="users")
router.register(r"tickets", TicketViewSet, basename="tickets")
router.register(r"order", OrderViewSet, basename="order")

urlpatterns = [
    path(r'', include(router.urls))
]
