from django.contrib import admin
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'api-auth/', include('rest_framework.urls')),
    path(r'get_token/', obtain_jwt_token, name='get_token'),
    path(r'', include('tickets_shop_api.urls'), name='tickets_shop_api')
]
