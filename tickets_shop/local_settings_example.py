import datetime
import os

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=5),
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'tickets_shop_api.utils.jwt_response_payload_handler'
}

# stripe setting
STRIPE_SECRET_KEY = 'secret_key'
STRIPE_PUBLISHABLE_KEY = 'publishable_key'

ACCOUNT_SECURITY_API_KEY = os.environ.get('ACCOUNT_SECURITY_API_KEY')
